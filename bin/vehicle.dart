void main(List<String> arguments) {
  Car car = Car("Mustang");
  print(car.getModelName());
  car.currentEngineStatus();
  car.engineStart();
  car.currentEngineStatus();
  car.engineStop();
  car.currentEngineStatus();
}

abstract class Vehicle {
  late String modelName;
  late String engineStatus;

  void engineStart() {
    print("Vehicle Engine Started.");
  }

  void engineStop() {
    print("Vehicle Engine Stopped.");
  }

  void currentEngineStatus();
}

abstract class CarFunction {
  void driveForward();
  void driveBackward();
  void blowHorn();
}

class Car extends Vehicle implements CarFunction {
  Car(String modelName) {
    this.modelName = modelName;
    engineStatus = "Off";
  }

  String getModelName() {
    return modelName;
  }

  @override
  void engineStart() {
    engineStatus = "On";
    print("Car : $modelName Engine Started.");
  }

  @override
  void engineStop() {
    engineStatus = "Off";
    print("Car : $modelName Engine Stopped.");
  }

  @override
  void blowHorn() {
    print("Car : $modelName Blow Horn.");
  }

  @override
  void driveBackward() {
    print("Car : $modelName Drives Backward.");
  }

  @override
  void driveForward() {
    print("Car : $modelName Drives Forward.");
  }
  @override
  void currentEngineStatus() {
    if (engineStatus == "On") {
      print("Car : $modelName Engine is On.");
    } else {
      print("Car : $modelName Engine is Off.");
    }

}
}